var myNameSpace = myNameSpace || {};

myNameSpace.SignUpModule = function(id, callback){

	// A NOTE ON THE PARAMS THAT GET PASSED IN
	// id - The id of the element that will contain this module
	// callback - The function that gets triggered the 'save' button is pressed (the input must be valid)

	var elem = document.getElementById(id);

	if(elem == null){
		throw new Error("The id passed in does not match an element");
	}

	elem.innerHTML = getTemplate();
	
	var txtName = elem.querySelector(".signUpName");
	var txtEmail = elem.querySelector(".signUpEmail");
	var btnSignUp = elem.querySelector(".btnSignUp");

	btnSignUp.onclick = function(){
		if(validate() && callback){
			var data = {name: txtName.value, email: txtEmail.value};
			callback(data);
		}
	};
	
	

	function getTemplate(){
		var html =  '<b>Your Name:</b><br /> \
					<input type="text" class="signUpName"/> \
					<span class="validation signup-name"></span><br/> \
					<b>Your Email:</b><br> \
					<input type="text" class="signUpEmail"/> \
					<span class="validation signup-email"></span><br /> \
					<button class="btnSignUp">Sign Up</button>';

		return html;
	}

	function validate(){
		
		var isValid = true;
		vSignupName = elem.querySelector(".signup-name");
		vSignupEmail = elem.querySelector(".signup-email");
		var focusOn = null;


		// clear out the validation messages
		var validationDivs = elem.querySelectorAll(".validation");
		
		for(var k in validationDivs){
			console.log(validationDivs[k]);
			validationDivs[k].innerHTML = "";
		}

		if(txtName.value == ""){
			isValid = false;
			vSignupName.innerHTML = "Please enter a name.";
			if(focusOn == null){
				focusOn = txtName;
			}
		}

		if(txtEmail.value == ""){
			isValid = false;
			vSignupEmail.innerHTML = "Please enter an email address.";
			if(focusOn == null){
				focusOn = txtEmail;
			}
		}else if(!validateEmail(txtEmail.value)){
			isValid = false;
			vSignupEmail.innerHTML = "The email address is not valid.";
			if(focusOn == null){
				focusOn = txtEmail;
			}
		}

		if(isValid == false){
			focusOn.focus();
		}

		return isValid;

	}

	function validateEmail(email) {
    	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    	return re.test(email);
	}

	function reset(){
		txtName.value = "";
		txtEmail.value = "";
	}

	

	function setName(nameValue){
		txtName.value = nameValue;
	}


	// RETURN THE PUBLIC API OF YOUR MODULE
	return {	
		setName: setName,
		reset: reset
	};

};

