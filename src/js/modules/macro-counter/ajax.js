var macro = macro || {};

macro.ajax = {
	send: function(options){

		var method = options.method;
		var url = options.url;
		var requestBody = options.requestBody;
		var http = new XMLHttpRequest();
		var callback = options.callback;
		var headers = options.headers; //object

		http.open(method, url);
		http.addEventListener('readystatechange', function() {
			if(http.readyState == 4 && http.status == 200) {
				//alert(http.responseText);
				callback(http.responseText);
			}else if(http.readyState == 4 && http.status !== 200){
			   	alert(http.status)
			}
		});
		if (headers){
			for(var key in someObject){
				http.setRequestHeaders(key, headers[key]);
			} 
		}
		http.send(requestBody);

		// send the XmlHttpRequest, and if the http.readyState == 4 and the http.status == 200
		// then invoke the callback function and pass the responseText as a param to it
		//callback("Replace this param with the responseText from the http object");

		// if the http.readyState == 4 and the http.status is NOT 200 then invoke the error method (defined below)
		// and pass the status as a param, like this...
		//this.error(http.status);

	},
	error: function(errMsg){
		alert(errMsg);
	}
};