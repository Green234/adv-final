var macro = macro || {};
macro.MacroDataAccess = function(){

// we'll use this function as our generic callback
		function handleResponse(response){
			alert(response);
		}

		//this module depends on the macro.ajax module
		var ajax = macro.ajax;
		if(!ajax){
			throw new Error("This module depends on the macro.ajax module")
		}

		function getAllMacros(callback){

			macro.ajax.send({
				callback: callback,
				url: "http://localhost/adv-topics-final/src/js/modules/macro/", 
				method:"GET"
			});
		}

		function getMacroById(id, callback){ 
			// TODO: use your macro.ajax module to send the proper request to the book web service
			// to get a book by it's id

			// Note: you can comment out this line of code...
			//callback("TODO: GET BOOK BY ID: " + id);

			macro.ajax.send({
				callback: callback,
				url: "http://localhost/adv-topics-final/src/js/modules/macro/" + id, 
				method:"GET"
			});
		}

		function postMacro(macro, callback){
			// TODO: use your macro.ajax module to send the proper request to the book web service
			// in order to add a book

			// Note: you can comment out this line...
			//callback("TODO: INSERT NEW BOOK: " + JSON.stringify(book));

			macro.ajax.send({
				callback: callback,
				url: "http://localhost/adv-topics-final/src/js/modules/macro/",
				requestBody:JSON.stringify(Macro),
				method:"POST"
			});
		}
	
		function putMacro(macro, callback){
			// TODO: use your macro.ajax module to send the proper request to the book web service
			// in order to add a book

			// Note: you can comment out this line...
			//callback("TODO: UPDATE A BOOK: " + JSON.stringify(book));
				macro.ajax.send({
				callback: callback,
				url: "http://localhost/adv-topics/book-web-service/macro/",
				requestBody:JSON.stringify(macro),
				method:"PUT"
			});
		}

		//return the public api for this module in an object
		return{
			getAllMacros:getAllMacros,
			getMacroById:getMacroById,
			postMacro:postMacro,
			putMacro:putMacro
		};

};

