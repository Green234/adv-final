<?php

class MacroDataAccess{
	
	private $link;

	/**
	* Constructor
	* @param $link 		The connection to the database
	*/
	function __construct($link){
		$this->link = $link;
	}


	/**
	* Get all macro
	* @param $order_by 		Optional - the sort order (fat or protein)
	* @return 2d array 		Returns an array of macro (each macro is an assoc array)
	*/
	function get_all_macros($order_by = null){
		// TODO: if the order_by param is not null we need to sort the macro (should be by fat or protein)
		$qStr = "SELECT	id, fat, protein, carb, food FROM macro";

		if($order_by == "fat" || $order_by == "protein"){
			$qStr .= " ORDER BY " . $order_by;
		}
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		$all_macros = array();

		while($row = mysqli_fetch_assoc($result)){

			$macro = array();
			$macro['id'] = htmlentities($row['id']);
			$macro['protein'] = htmlentities($row['protein']);
			$macro['fat'] = htmlentities($row['fat']);
			$macro['carb'] = htmlentities($row['carb']);
			$macro['food'] = htmlentities($row['food']);

			$all_macros[] = $macro;
		}

		return $all_macros;
	}


	/**
	* Get a macro by its ID
	* @param $id 			The ID of the macro to get
	* @return array 		An assoc array that has keys for each property of the macro
	*/
	function get_macro_by_id($id){
		
		// TODO: if the order_by param is not null we need to sort the macro (should be by fat or protein)
		$qStr = "SELECT	id, fat, protein, carb, food FROM macro WHERE id = " . mysqli_real_escape_string($this->link, $id);

				
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result->num_rows == 1){
			$row = mysqli_fetch_assoc($result);

			$macro = array();
			$macro['id'] = htmlentities($row['id']);
			$macro['protein'] = htmlentities($row['protein']);
			$macro['fat'] = htmlentities($row['fat']);
			$macro['carb'] = htmlentities($row['carb']);
			$macro['food'] = htmlentities($row['food']);
			return $macro;

		}else{
			return null;
		}
			
	}


	function insert_macro($macro){

		// prevent SQL injection
		$macro['fat'] = mysqli_real_escape_string($this->link, $macro['fat']);
		$macro['protein'] = mysqli_real_escape_string($this->link, $macro['protein']);
		

		$qStr = "INSERT INTO macro (
					fat,
					protein,
					carb,
					food
				) VALUES (
					'{$macro['fat']}',
					'{$macro['protein']}',
					'{$macro['carb']}',
					'{$macro['food']}'
				)";
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result){
			// add the macro id that was assigned by the data base
			$macro['id'] = mysqli_insert_id($this->link);
			// then return the macro
			return $macro;
		}else{
			$this->handle_error("unable to insert macro");
		}

		return false;
	}



	function update_macro($macro){

		// prevent SQL injection
		$macro['id'] = mysqli_real_escape_string($this->link, $macro['id']);
		$macro['fat'] = mysqli_real_escape_string($this->link, $macro['fat']);
		$macro['protein'] = mysqli_real_escape_string($this->link, $macro['protein']);
		$macro['carb'] = mysqli_real_escape_string($this->link, $macro['carb']);
		$macro['food'] = mysqli_real_escape_string($this->link, $macro['food']);

		$qStr = "UPDATE macro SET fat='{$macro['fat']}', protein='{$macro['protein']}, carb='{$macro['carb']}, food='{$macro['food']}' WHERE id = " . $macro['id'];
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result){
			return $macro;
		}else{
			$this->handle_error("unable to update macro");
		}

		return false;
	}
	


}