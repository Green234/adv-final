var macro = macro || {};

macro.MacroModuleDetails = function(id, callback){

	// A NOTE ON THE PARAMS THAT GET PASSED IN
	// id - The id of the element that will contain this module
	// callback - The function that gets triggered the 'save' button is pressed (the input must be valid)

	var elem = document.getElementById(id);

	if(elem == null){
		throw new Error("The id passed in does not match an element");
	}

	elem.innerHTML = getTemplate();
	
	var txtProtein = elem.querySelector(".protein");
	var txtFat = elem.querySelector(".fat");
	var txtCarb = elem.querySelector(".carb");
	var txtFood = elem.querySelector(".food");
	var btnAdd = elem.querySelector(".btnAdd");
	var btnSave = elem.querySelector(".btnSave");
	var btnDelete = elem.querySelector(".btnDelete");


	btnAdd.onclick = function(){
		if(validate() && callback){
			var data = {protein: txtProtein.value, fat: txtFat.value, carb: txtCarb.value, food: txtFood.value};
			callback(data);
		}
	};
	
	

	function getTemplate(){
		var html =  '<b>Protein:</b><br /> \
					<input type="text" class="protein"/> \
					<span class="validation proteins"></span><br/> \
					<b>Fats:</b><br /> \
					<input type="text" class="fat"/> \
					<span class="validation fats"></span><br/> \
					<b>Carbohydrates:</b><br /> \
					<input type="text" class="carb"/> \
					<span class="validation carbs"></span><br/> \
					<b>Food:</b><br /> \
					<input type="text" class="food"/> \
					<span class="validation foods"></span><br/> \
					<button class="btnAdd">Add</button> \
					<button class="btnSave">Save</button> \
					<button class="btnDelete">Delete</button>';

		return html;
	}

	function validate(){
		
		var isValid = true;
		vProtein = elem.querySelector(".proteins");
		vFat = elem.querySelector(".fats");
		vCarb = elem.querySelector(".carbs");
		vFood = elem.querySelector(".foods");
		var focusOn = null;


		// clear out the validation messages
		var validationDivs = elem.querySelectorAll(".validation");
		
		for(var k in validationDivs){
			console.log(validationDivs[k]);
			validationDivs[k].innerHTML = "";
		}

		if(txtProtein.value == ""){
			isValid = false;
			vProtein.innerHTML = "Please enter your Protein.";
			if(focusOn == null){
				focusOn = txtProtein;
			}
		}

		if(txtFat.value == ""){
			isValid = false;
			vFat.innerHTML = "Please enter your Fat.";
			if(focusOn == null){
				focusOn = txtFat;
			}
		}

		if(txtCarb.value == ""){
			isValid = false;
			vCarb.innerHTML = "Please enter your Carbohydrate.";
			if(focusOn == null){
				focusOn = txtCarb;
			}
		}

		if(txtFood.value == ""){
			isValid = false;
			vFood.innerHTML = "Please enter your Food.";
			if(focusOn == null){
				focusOn = txtFood;
			}
		}

		if(isValid == false){
			focusOn.focus();
		}

		return isValid;

	}

	function reset(){
		txtProtein.value = "";
		txtFat.value = "";
		txtCarb.value = "";
		txtFood.value = "";
	}

	function setMacro(nameValue){
		txtProtein.value = nameValue;
		txtFat.value = nameValue;
		txtCarb.value = nameValue;
		txtFood.value = nameValue;
	}


	// RETURN THE PUBLIC API OF YOUR MODULE
	return {	
		setMacro: setMacro,
		reset: reset
	};

};

