var acme = acme || {};

macro.MacroModuleList = {
	
	init: function(options){
		// Instance Vars
		var target = options.target;  
		var data = options.data;  
	  	var selectCallback = options.selectCallback;
	  	divId = target.getAttribute("ID");

	  	function setUpList(){
	  		target.innerHTML = "";
	  		var macroList = document.createElement("UL");
	  		for(var x = 0; x < data.length; x++){

		      var macro = data[x];
		      var li = document.createElement("LI");
		      var div = document.createElement("DIV");
		      var btn = document.createElement("BUTTON");
		      var macroItem = document.createElement("LI");
		      var protein = document.createElement("LI");

		      li.setAttribute("style", "list-style:none");

		      btn.setAttribute("type", "button");
			  btn.setAttribute("value", "edit");
			  btn.setAttribute("data-macroId", macro.id);

		      protein.innerHTML = macro.protein;
		      btn.innerHTML = "Edit";
		      macroItem.innerHTML = macro.fat;

		      li.appendChild(div);
		      div.appendChild(macroItem);
		      div.appendChild(protein);
		      div.appendChild(btn);
		      macroList.appendChild(li);
		    }
		    return macroList;
	  	}

	    function setMacro(macro){
	    	data = macro;
	    	if(data){
	    		target.appendChild(setUpList());
	    	}
	    }

	    function getMacroById(id){
	    	for(var x = 0; x < data.length; x++){
	    		if(id == data[x].id){
	    			return data[x];
	    		}
	    	}
	    }

	    function refresh(){
	    	target.appendChild(setUpList());
	    }

	    target.addEventListener('click', function(e){
			var macroId = e.target.getAttribute("data-macroId");
				if(macroId){
					var macro = getMacroById(macroId);
					selectCallback(macro);
				}
		});

		return{
			setMacro: setMacro,
			refresh: refresh
		};
	}
};