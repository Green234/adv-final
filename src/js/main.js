window.addEventListener("load", function(){

	var da = macro.MacroDataAccess();

	var macroModuleDetails = macro.MacroModuleDetails("macro-details-container", function(data){
		alert("This is the callback!\nTODO: send this data to the server for saving!")
		console.log(data);
	});

	da.getAllMacros(function(response){
		var macro = JSON.parse(response);
		macroList.setMacros(macro);
	});

	var macroList = macro.MacroModuleList.init({
		target: document.getElementById("macro-list-container"),
		selectCallback: function(macro){
			macroDetails.setMacro(macro);
		}
	});

});